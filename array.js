
dd.array = Object();


dd.array.each = function(arr,fn,scope) {
  if (scope) {
    fn = fn.bind(scope)
  }
  if (!arr || arr == 'undefined') {
    return;
  }

  for (var i = 0; i < arr.length; i++) {
    fn(arr[i],i);
  }
};

dd.array.contains = function(arr, obj) {
  var i = arr.length;
  while (i--) {
    if(arr[i]==obj) {
      return true;
    }
  }
  return false;
};

dd.array.count = function(arr) {
  return arr.length;
};

