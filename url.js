dd.url = Object();

dd.url.UrlHandler = function() {
  this.protocol = document.location.protocol
  this.update();
  dd.events.listen(window,'hashchange',this.hashChanged,this);
};
dd.extend(dd.url.UrlHandler,dd.events.EventTarget);

dd.url.UrlHandler.prototype.update = function() {
  this.protocol = document.location.protocol
  this.url = this.protocol + "//" + document.location.hostname + document.location.pathname;
  this.path = document.location.pathname;
  this.host = document.location.hostname;
  this.hashString = document.location.hash;
  this.hash = Array();
  if (dd.string.startsWith(this.hashString,"#")) {
    this.hash = this.hashString.substring(1).split("/");
  }
};

dd.url.UrlHandler.prototype.setHash = function(str) {
  document.location.hash = str;
}

dd.url.UrlHandler.prototype.back = function() {
  window.history.back();
}

dd.url.UrlHandler.prototype.hashChanged = function() {
  this.update();
  this.dispatchEvent({'type':'HASH_CHANGED','value':this.hash});
};
