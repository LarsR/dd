dd.events = Object();


dd.events.EventHandler = function() {
  this.listeners = Array();
}

dd.events.EventHandler.prototype.listen = function(obj,key,fn,scope) {
  if(scope) {
    fn = fn.bind(scope);
  }
  if(typeof key == 'object') {
    dd.array.each(key,function(k) {
      obj.addEventListener(k,fn);
      this.listeners.push({'key':k,'obj':obj,'fn':fn});
    },this);
  }
  else {

    obj.addEventListener(key,fn);
    this.listeners.push({'key':key,'obj':obj,'fn':fn});
  }
}
dd.events.EventHandler.prototype.listenOnce = function(obj,key,fn,scope) {
  if(scope) {
    fn = fn.bind(scope);
  }
  if ( typeof key == 'array') {
    dd.array.each(key,function(k){
      obj.addEventListener(k,fn);
      this.listeners.push({'key':k,'obj':obj,'fn':fn});
    },this);
  }
  else {
    obj.addEventListener(key,fn);
    this.listeners.push({'key':key,'obj':obj,'fn':fn});
  }
}

dd.events.EventHandler.prototype.removeAll = function() {
  dd.array.each(this.listeners,function(value) {
    value['obj'].removeEventListener(value['key'],value['fn']);
  });
  this.listeners = Array();
}


dd.events.listen = function(obj,key,fn,scope) {
  if(scope){
    fn = fn.bind(scope);
  }
  obj.addEventListener(key,fn);
}

dd.events.EventTarget = function() {
  this.listeners = {};
};

dd.events.EventTarget.prototype.listen = function(key,fn,scope) {
  if(Object.prototype.toString.call( key ) === '[object Array]'){
    dd.array.each(key,function(value){
      if (!this.listeners[value]) {
        this.listeners[value] = [];
      }
      this.listeners[value].push({'fn':fn,'scope':scope});
    },this);
  }
  else {
    if (!this.listeners[key]) {
      this.listeners[key] = [];
    }
    this.listeners[key].push({'fn':fn,'scope':scope});
  }
};

dd.events.EventTarget.prototype.dispatchEvent = function(data) {
  var key = "";
  if (typeof data == 'object'){
    key = data['type'];
  }
  if (this.listeners[key]) {
    this.data = data;
    dd.array.each(this.listeners[key],function(obj){
      var scope = obj['scope'];
      var func = obj['fn'].bind(scope);
      func(data);
    },this);

  }
};



