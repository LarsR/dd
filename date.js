dd.date = Object();

dd.date.Now = function() {
  var datetime = new dd.date.Date();
  return datetime;
};
dd.date.Date = function() {
  this.datetime = new Date();
};

dd.date.Date.prototype.parse = function(format) {
  var month_ = (this.datetime.getMonth() < 10)?'0'+ (this.datetime.getMonth()+1):this.datetime.getMonth()+1;
  var date_ = (this.datetime.getDate() < 10)?'0'+ this.datetime.getDate():this.datetime.getDate();
  var hour_ = (this.datetime.getHours() < 10)?'0'+ this.datetime.getHours():this.datetime.getHours();
  var minute_ = (this.datetime.getMinutes() < 10)?'0'+ this.datetime.getMinutes():this.datetime.getMinutes();
  format = format.replace('YYYY',this.datetime.getFullYear());
  format = format.replace('mm',month_);
  format = format.replace('dd',date_);
  format = format.replace('HH',hour_);
  format = format.replace('ii',minute_);
  return format;
}





