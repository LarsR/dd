dd.dom = Object();

dd.dom.getElement = function(id) {
  var el = document.getElementById(id);
  return el;
};

dd.dom.createElement = function(tag,attributes,content) {
  var dom = document.createElement(tag);
  if (typeof attributes == 'object') {
    dd.object.each(attributes,function(value,key){
      dom.setAttribute(key,value);
    });
  }
  if (content ) {
    dd.dom.append(content,dom);
  }
  return dom;
};

dd.dom.insertFirst = function(srcNode,targetNode) {
  if (targetNode.childNodes.length > 0 ) {
    targetNode.insertBefore(srcNode, targetNode.firstChild);
  }
  else {
    targetNode.appendChild(srcNode);
  }
}

dd.dom.setContent = function(srcNode,targetNode) {
  dd.dom.removeChildren(targetNode);
  if (typeof srcNode == 'string') {
    targetNode.appendChild(document.createTextNode(srcNode));
  }
  else if(Object.prototype.toString.call( srcNode ) === '[object Array]') {
    dd.array.each(srcNode,function(value){
      dd.dom.append(value,targetNode);
    });
  }
  else if (srcNode) {
    targetNode.appendChild(srcNode);
  }
  else{
    console.info("unknown type");
  }
}


dd.dom.remove = function (domNode) {
  if (domNode.parentNode) {
    domNode.parentNode.removeChild( domNode );
  }
}

dd.dom.append = function(srcNode,targetNode) {
  if (typeof srcNode == 'string') {
    targetNode.appendChild(document.createTextNode(srcNode));
  }
  else if(Object.prototype.toString.call( srcNode ) === '[object Array]') {
    dd.array.each(srcNode,function(value){
      dd.dom.append(value,targetNode);
    });
  }
  else if (srcNode) {
    targetNode.appendChild(srcNode);
  }
  else{
    console.info("unknown type");
  }
}

dd.dom.removeChildren = function(element) {
  while (element.firstChild) {
    element.removeChild(element.firstChild);
  }
}
dd.dom.getParent = function(element) {
  return element.parentNode;
}



dd.dom.getClasses = function(element) {
  var classes = element.getAttribute('class').split(" ");
  return classes;
};

dd.dom.setClass = function(element,cls) {
 element.classList = cls;
};

dd.dom.swapClass = function(element,oldcls,newcls) {

 element.classList.add(newcls);
 element.classList.remove(oldcls);
};

dd.dom.addClass = function(element,cls) {
 element.classList.add(cls);
};

dd.dom.removeClass = function(element,cls) {
  element.classList.remove(cls);
};

dd.dom.hasClass = function(element,cls) {
  return element.classList.contains(cls);
};

dd.dom.toggleClass = function(element,cls) {
  if (element.classList.contains(cls)) {
    dd.dom.removeClass(element,cls);
  }else{
    dd.dom.addClass(element,cls);
  }
};


dd.dom.handleClass = function(element,cls,add) {
  if (add) {
    element.classList.add(cls);
  }
  else {
    element.classList.remove(cls);
  }
}
dd.dom.listen = function(obj,key,fn,scope) {
  if(scope){
    fn = fn.bind(scope);
  }
  obj.addEventListener(key,fn);
}
